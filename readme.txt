
INTRODUCTION

------------

Code Test: Banking is a finantial program used to simulate transactions between accounts from same and different banks.


REQUIREMENTS

------------

This program requires to install ruby.

Latest version --> 2.4.0
Download here --> https://www.ruby-lang.org/en/downloads/


RUN

---

Open --> Command Line --> Go to path and run ruby main.rb

Add paramethers between parenthesis ().

Agent.new --> to create a new Agent, who is gonna control transactions to work.

Bank.new("BBVA") --> to create a new bank with the paramether "name"

Account.new("Emma", 12000, bank1) --> to create a new account, attaching a name, initial account balance and its bank.

Transaction.new --> to create all transactions history list.

Transfer.new(acc1,acc2,15000, agent, acc1.bank, acc2.bank, transaction_history) --> to create a new transfer with the parameters:
accounts, (destination account and origin account), quantity money, 2 banks, destination account bank and origin account bank, transactions history.

transaction_history.show_history --> to show all transactions full history list.

acc1.show_transfer_history --> to show all transactions history of the account: "acc1"

acc1.balance --> to show the balance of the account: acc1

bank1.show_bank_transactions --> to show all transactions history of the bank: "bank1"


FAQ

---

Q: If you don't have enough founds to make a transference, will it do it?

A: No, to prevent negative balance in accounts, you can't make transfers for a higher value of your account balance.

Q: Can transfers between banks fail?

A: No, we created an agent to prevent transfers failures.


