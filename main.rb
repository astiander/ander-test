Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }

agent = Agent.new
bank1 = Bank.new("BBVA")
bank2 = Bank.new("Caja Rural")
acc1 = Account.new("Emma", 12000, bank1)
acc2 = Account.new("Jim", 12500, bank1)
acc3 = Account.new("Jaime", 2000, bank2)
acc4 = Account.new("Pepe", 14000, bank2)
puts "INICIO: \n#{acc1.name} tiene: #{acc1.account_money} euros en el banco #{acc1.bank.name}\n#{acc2.name} tiene: #{acc2.account_money} euros en el banco #{acc2.bank.name}
#{acc3.name} tiene: #{acc3.account_money} euros en el banco #{acc3.bank.name}\n#{acc4.name} tiene: #{acc4.account_money} euros en el banco #{acc4.bank.name}"
transaction_history = Transaction.new 
transfer1 = Transfer.new(acc1,acc2,15000, agent, acc1.bank, acc2.bank, transaction_history)
transaction_history.show_history
acc1.show_transfer_history
transfer2 = Transfer.new(acc3,acc1,2550, agent, acc3.bank, acc1.bank, transaction_history)
transfer3 = Transfer.new(acc3,acc1,550, agent, acc3.bank, acc1.bank, transaction_history)
transfer4 = Transfer.new(acc2,acc4,9550, agent, acc2.bank, acc4.bank, transaction_history)
acc1.balance
transaction_history.show_history
bank1.show_bank_transactions
