class Account
  attr_reader :name, :bank
  attr_accessor :account_money

  def initialize(name, money, bank)
    @name = name
    @account_money = money
    @bank = bank
    @transactions = []
  end

  def add_transaction(transaction)
    @transactions << transaction
  end

  def show_transfer_history
    puts "#{@name} transactions history:"
    if @transactions
      n = 0
      @transactions.each do |transaction|
        n += 1
        puts "Transaction #{n} -> #{transaction.object_id}: "
        puts "From:#{transaction.origin_account.name}"
        puts "To:#{transaction.destination_account.name}"
        puts "Quantity:#{transaction.money}"
      end
    else puts "#{@name} no ha realizado ninguna transferencia"
    end
  end

  def balance
    puts "#{name} current balance: #{@account_money}"
  end
end
