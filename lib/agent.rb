class Agent
  def check_transaction(destination_account, origin_account, money)
    @money = money
    @destination_account = destination_account
    @origin_account = origin_account
    @try = 0
    transaction_error_chance
    if money > 1000
      big_quantity_transactions
    else
      destination_account.account_money += money
      origin_account.account_money -= money +5
    end
  end

  def transaction_error_chance
    while rand(100) < 30
      @try += 1
      puts "Transaction has failed, trying again. Failed attemps: #{@try}"
    end
    puts "Transaction succesful"
  end

  def big_quantity_transactions
    puts @money
    puts "Transactions between banks cannot be greater than 1000 euros, we will divide it in 1000 euros transactions with 5euros comission each"
    rest = @money % 1000
    number_of_transactions = ((@money - rest) / 1000)
    current_number_of_transactions = 0
    total_comision = 5
    while current_number_of_transactions <= number_of_transactions
      total_comision += 5
      current_number_of_transactions += 1
      transaction_error_chance
      @destination_account.account_money += 1000
      @origin_account.account_money -= 1000 +5
    end
    @destination_account.account_money += rest
    @origin_account.account_money -= rest +5
    puts "Operation divided in total transactions: #{number_of_transactions +1}"
    puts "Total comision: #{total_comision} euros"
  end
end
