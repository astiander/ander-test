class Transfer
  attr_reader :destination_account, :origin_account, :money

  def initialize(destination_account, origin_account, money, agent, bank1, bank2, transaction_history)
    @destination_account = destination_account
    @origin_account = origin_account
    @money = money
    @transaction_history = transaction_history
    @agent = agent
    @bank1 = bank1
    @bank2 = bank2
    make_transaction
  end

  def make_transaction
    if @destination_account.account_money < @money
      puts "Transaction failed: #{@destination_account.name} has not enough founds"
    else
      if @destination_account.bank.object_id == origin_account.bank.object_id
        @destination_account.account_money += @money
        @origin_account.account_money -= @money
      else
        @agent.check_transaction(@destination_account,@origin_account,@money)
      end
    puts "Transaccion realizada: #{@money} de la cuenta #{@origin_account.name} a la cuenta de #{@destination_account.name}"
    transaction_history_update(self)
    end
  end

  def transaction_history_update(transfer)
    @destination_account.add_transaction(transfer)
    @origin_account.add_transaction(transfer)
    @transaction_history.add_transaction(transfer)
    @bank1.add_transaction(transfer)
    @bank2.add_transaction(transfer)
  end
end
