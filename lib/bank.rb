class Bank
  attr_reader :name, :bank

  def initialize(name)
    @name = name
    @transactions = []
  end

  def add_transaction(transaction)
    @transactions << transaction
  end

  def show_bank_transactions
    n = 0
    puts "Transaction history in bank #{name}"
    @transactions.each do |transaction|
      n += 1
      puts "............"
      puts "Number of transactions: #{n}"
      puts "Transaction id: #{transaction.object_id}"
      puts "Account 1: #{transaction.destination_account.name}"
      puts "Account 2:#{transaction.origin_account.name} "
      puts "Transfered money: #{transaction.money}"
    end
    if @transactions.length == 0
      puts "No se han realizado transferencias."
    end
  end
end
